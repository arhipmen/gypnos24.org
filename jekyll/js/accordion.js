window.onload = () => {
  function slide(link) {

    let down = function (callback, time) {
          let subMenu = link.nextElementSibling;
          let height = subMenu.clientHeight;
          let part = height / 100;

          let paddingTop = parseInt(window.getComputedStyle(subMenu, null).getPropertyValue('padding-top'));
          let paddingBottom = parseInt(window.getComputedStyle(subMenu, null).getPropertyValue('padding-bottom'));
          let paddingTopPart = parseInt(paddingTop) / 50;
          let paddingBottomPart = parseInt(paddingBottom) / 30;

          (function innerFunc(i, t, b) {

            subMenu.style.height = i + 'px';

            i += part;

            if(t < paddingTop) {

              t += paddingTopPart;
              subMenu.style.paddingTop = t + 'px';

            } else if(b < paddingBottom) {

              b += paddingBottomPart;
              subMenu.style.paddingBottom = b + 'px';
            }

            if(i < height) {

              setTimeout(function() {

                innerFunc(i, t, b);

              }, time / 100);

            } else {

              subMenu.removeAttribute('style');
              callback();
            }

          })(0, 0, 0);
        },

        up = function (callback, time) {

          let subMenu = link.nextElementSibling;
          let height = subMenu.clientHeight;
          let part = subMenu.clientHeight / 100;
          let paddingTop = parseInt(window.getComputedStyle(subMenu).paddingTop);
          let paddingBottom = parseInt(window.getComputedStyle(subMenu).paddingBottom);
          let paddingTopPart = parseInt(paddingTop) / 30;
          let paddingBottomPart = parseInt(paddingBottom) / 50;

          (function innerFunc(i, t, b) {

            subMenu.style.height = i + 'px';

            i -= part;
            i = i.toFixed(2);

            if(b > 0) {

              b -= paddingBottomPart;
              b = b.toFixed(1);
              subMenu.style.paddingBottom = b + 'px';

            } else if(t > 0) {

              t -= paddingTopPart;
              t = t.toFixed(1);
              subMenu.style.paddingTop = t + 'px';
            }

            if(i > 0) {

              setTimeout(function() {

                innerFunc(i, t, b);

              }, time / 100);

            } else {

              subMenu.removeAttribute('style');
              callback();
            }

          })(height, paddingTop, paddingBottom);
        }

    return { down, up }
  }

  let accordion = (function() {

    let menu = document.querySelectorAll('#accordion');
    let activeClass = 'accordion__link_active';
    let arr = [];
    let timer = 100;

    for(let i = 0; i < menu.length; i++) {

      for(let p = 0; p < menu[i].children.length; p++) {

        let link = menu[i].children[p].firstElementChild;

        if(link.classList.contains(activeClass)) {
          arr[i] = link;
        }
      }
    }

    function accordionInner(i) {

      let clicked = false;

      menu[i].addEventListener('click', (e) => {

        if(e.target.tagName === 'A' && !clicked) {

          clicked = true;

          if(e.target.classList.contains(activeClass)) {

            slide(e.target).up(function() {

              clicked = false;

              e.target.classList.remove(activeClass);

              console.log('slide up of accordion ' + i + ' is done');

            }, timer);

          } else {

            if(arr.length > 0) {

              slide(arr[i-1]).up(function() {

                arr[i-1].classList.remove(activeClass);

                arr[i-1] = e.target;

                console.log('slide up of accordion ' + i + ' is done');

              }, timer);
            }

            e.target.classList.add(activeClass);

            slide(e.target).down(function() {

              clicked = false;

              arr[i-1] = e.target;

              console.log('slide down of accordion ' + i + ' is done');

            }, timer);
          }
        }
      });

      i++;

      if(i < menu.length) { accordionInner(i); }

    } accordionInner(0);
  })();
};